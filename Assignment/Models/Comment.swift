//
//  Comment.swift
//  Assignment
//
//  Created by Neha Jain on 18/01/20.
//

import UIKit

class Comment: NSObject {
    var votesCount: Int = 0
    var name: String?
    var commentDescription: String?
    var thumbnailImgaeURLString: String?
    var uid: Int?
    var user: [User] = []
    var dateOfposting = Date()
    
    init(raw: [String: Any]) {
        if let value = raw["votes"] as? Int {
            votesCount = value
        }
        
        if let value = raw["id"] as? Int {
            uid = value
        }
        
        if let userDetails = raw["user"] as? [String: Any] {
            user = User.models(from: [userDetails])
        }
        
        if let value = raw["body"] as? String {
            commentDescription = value
        }
        
        if let value = raw["thumbnail"] as? [String: Any], let urlString = value["image_url"] as? String {
            thumbnailImgaeURLString = urlString
        }
        
        if let value = raw["name"] as? String {
            name = value
        }
    }
    
    static func models(from rawArray: [[String: Any]]) -> [Comment] {
        var list: [Comment] = []
        for raw in rawArray {
            list.append(Comment(raw: raw))
        }
        return list
    }
}
