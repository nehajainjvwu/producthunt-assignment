//
//  User.swift
//  Assignment
//
//  Created by Neha Jain on 18/01/20.
//

import UIKit

class User: NSObject {
    var name: String?
    var thumbnailImgaeURLString: String?
    
    init(raw: [String: Any]) {
        if let value = raw["image_url"] as? [String: Any], let urlString = value["48px"] as? String {
            thumbnailImgaeURLString = urlString
        }
        
        if let value = raw["name"] as? String {
            name = value
        }
    }
    
    static func models(from rawArray: [[String: Any]]) -> [User] {
        var list: [User] = []
        for raw in rawArray {
            list.append(User(raw: raw))
        }
        return list
    }
}
