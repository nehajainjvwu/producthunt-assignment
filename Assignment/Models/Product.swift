//
//  Product.swift
//  Assignment
//
//  Created by Neha Jain on 18/01/20.
//

import UIKit
import RealmSwift

class Product: Object {
    @objc dynamic var votesCount: Int = 0
    @objc dynamic var commentsCount: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var tagline: String?
    @objc dynamic var slug: String?
    @objc dynamic var thumbnailImgaeURLString: String?
    @objc dynamic var uid: Int = 0
    @objc dynamic var firstTopic: String?
    
    init(raw: [String: Any]) {
        if let value = raw["votes_count"] as? Int {
            votesCount = value
        }
        
        if let value = raw["id"] as? Int {
            uid = value
        }
        
        if let value = raw["comments_count"] as? Int {
            commentsCount = value
        }
        
        if let value = raw["slug"] as? String {
            slug = value
        }
        
        if let value = raw["name"] as? String {
            name = value
        }
        
        if let value = raw["thumbnail"] as? [String: Any], let urlString = value["image_url"] as? String {
            let components = urlString.split(separator: "?")
            thumbnailImgaeURLString = components.first?.appending("?auto=format&auto=compress&codec=mozjpeg&cs=strip&w=80&h=80&fit=crop")
        }
        
        if let value = raw["tagline"] as? String {
            tagline = value
        }
        
        if let rawTopics = raw["topics"] as? [[String: Any]], let rawFirstTopic = rawTopics.first {
            firstTopic = rawFirstTopic["name"] as? String
        }
    }
    
    required init() {
        super.init()
    }
    
    static func models(from rawArray: [[String: Any]]) -> [Product] {
        var list: [Product] = []
        for raw in rawArray {
            list.append(Product(raw: raw))
        }
        return list
    }
}
