//
//  Service.swift
//  GrimLock
//
//  Created by Neha Jain on 08/03/19.
//  Copyright © 2019 Neha Jain. All rights reserved.
//

import Foundation
import Alamofire

typealias OnResponse = (_ response: [String: Any]?, _ error: Error?, _ responseCode: Int?) -> Void

class Service {
    func call(_ path: String, parameters: [String: Any]?, type: HTTPMethod, onResponse: @escaping OnResponse) {
        let urlString = "https://api.producthunt.com/v1/" + path
        AF.request(urlString, method: type, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders(["Authorization": "Bearer e5B6npHbGL1nGhsBO9z2P-_6SQHY_80JSYUhOUzFPX0"])).responseJSON { response in
            switch response.result {
            case .success(let result):
                if let json = result as? [String: Any] {
                    onResponse(json, nil, response.response?.statusCode)
                } else {
                    onResponse(nil, NSError(domain: "", code: 0, userInfo: ["message": "No data"]), response.response?.statusCode)
                }
            case .failure(let error):
                if let data = response.data {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
                            onResponse(json, error, error.asAFError?.responseCode)
                        } else {
                            onResponse(nil, error, error.asAFError?.responseCode)
                        }
                    } catch _ as NSError {
                        onResponse(nil, error, error.asAFError?.responseCode)
                    }
                } else {
                    onResponse(nil, error, error.asAFError?.responseCode)
                }
            }
        }
    }
}
