//
//  CommentTableViewCell.swift
//  Assignment
//
//  Created by Neha Jain on 18/01/20.
//

import UIKit

class CommentTableViewCell: UITableViewCell, NibLoadableView, ReusableView {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var upVoteCountLabel: UILabel!
    
    var comment: Comment? {
        didSet {
            if let comment = comment {
                loader.stopAnimating()
                cardView.isHidden = false
                
                userNameLabel.text = comment.user.first?.name
                if let urlString = comment.user.first?.thumbnailImgaeURLString {
                    userImageView.sd_setImage(with: URL(string: urlString), completed: nil)
                }
                commentLabel.text = comment.commentDescription
                upVoteCountLabel.text = "\(comment.votesCount)"
            } else {
                loader.startAnimating()
                cardView.isHidden = true
            }
        }
    }
}

