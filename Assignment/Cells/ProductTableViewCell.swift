//
//  ProductTableViewCell.swift
//  Assignment
//
//  Created by Neha Jain on 18/01/20.
//

import UIKit
import SDWebImage

class ProductTableViewCell: UITableViewCell, NibLoadableView, ReusableView {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var taglineLabel: UILabel!
    @IBOutlet weak var upVotesView: UIView!
    @IBOutlet weak var upVoteCountLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var topicLabel: UILabel!
    
    var product: Product? {
        didSet {
            self.productImageView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
            if let urlString = product?.thumbnailImgaeURLString {
                productImageView.sd_setImage(with: URL(string: urlString)) { (_, _, _, _) in
                    self.productImageView.backgroundColor = nil
                }
            } else {
                productImageView.image = nil
            }
            titleLabel.text = product?.name
            taglineLabel.text = product?.tagline
            upVoteCountLabel.text = "\(product?.votesCount ?? 0)"
            commentCountLabel.text = "\(product?.commentsCount ?? 0)"
            topicLabel.text = product?.firstTopic            
        }
    }
}
