//
//  ProductCommentsViewController.swift
//  Assignment
//
//  Created by Neha Jain on 18/01/20.
//

import UIKit

class ProductCommentsViewController: UIViewController {
    var product: Product! // Can be Implicity Unwrapped Optional coz this VC will open only after setting `product`
    var comments: [Comment] = []
    let perPage = 5
    var currentPage = 1
    var mightHaveNextPage = true
    var isLoadingComments = true

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = product?.name
        setupTableView()
        loadComments()
    }
    
    func loadComments() {
        isLoadingComments = true
        let path = "comments?search[post_id]=" + "\(product.uid)" + "&per_page=" + "\(perPage)" + "&page=" + "\(currentPage)"
        Service().call(path, parameters: nil, type: .get) { (response, error, responseCode) in
            if let response = response, let rawComments = response["comments"] as? [[String: Any]] {
                let newComments = Comment.models(from: rawComments)
                self.comments.append(contentsOf: newComments)
                self.mightHaveNextPage = newComments.count == self.perPage
                print("Loaded comments: \(newComments.count)")
                print("Total comments now: \(self.comments.count)")
                print("mightHaveNextPage: \(self.mightHaveNextPage)")
                self.isLoadingComments = false
                self.tableView.reloadData()
            }
        }
    }
    
    func setupTableView() {
        tableView.register(CommentTableViewCell.self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 0)
    }
}

extension ProductCommentsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count + (mightHaveNextPage ? 1: 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CommentTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        if indexPath.row < comments.count {
            cell.comment = comments[indexPath.row]
        } else {
            cell.comment = nil
            print("cell.comment was Nil")
            if !isLoadingComments && mightHaveNextPage {
                currentPage = currentPage + 1
                print("cell.comment was Nil so loading : \(currentPage)")
                loadComments()
            }
        }
        return cell
    }
}
