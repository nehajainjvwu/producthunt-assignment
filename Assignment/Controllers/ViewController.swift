//
//  ViewController.swift
//  Assignment
//
//  Created by Neha Jain on 18/01/20.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var datePickerContainerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var noProductLabel: UILabel!
    
    lazy var dateBarButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(dateButtonTouched))
    
    var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(reloadProducts), for: .valueChanged)
        return control
    }()
    
    var isInErrorState = false
    
    var products: [Product] = [] {
        didSet {
            searchedProducts = products
            searchBar.text = nil
        }
    }
    
    var searchedProducts: [Product] = [] {
        didSet {
            tableView.reloadData()
            noProductLabel.isHidden = searchedProducts.count != 0
            
            if let searchText = searchBar.text, searchText.count > 0 {
                noProductLabel.text = "No products for \"\(searchText)\""
            } else if isInErrorState {
                if selectedDate.isEqualDayWise(to: Date()) {
                    noProductLabel.text = "Try loading again"
                } else {
                    noProductLabel.text = "Try loading again, offline posts Might be available for today"
                }
            } else {
                noProductLabel.text = "No products"
            }
        }
    }
    
    var selectedDate = Date() {
        didSet {
            // Simple date comparision won't work as it would compare till millisecond level
            if selectedDate.isEqualDayWise(to: Date()) {
                dateBarButton.title = "  Today  "
            } else {
                dateBarButton.title = "  " + selectedDate.asString + "  "
            }

            loadProducts()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Products"
        navigationItem.rightBarButtonItem = dateBarButton
        setupDatePicker()
        setupTableView()
        selectedDate = Date()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ProductCommentsViewController,
            let product = sender as? Product {
            destination.product = product
        }
    }
}

extension ViewController {
    @objc func dateButtonTouched() {
        datePicker.setDate(selectedDate, animated: false)
        datePickerContainerView.isHidden = false
        view.endEditing(true)
    }
    
    @IBAction func doneDatePicker() {
        datePickerContainerView.isHidden = true
        
        if !datePicker.date.isEqualDayWise(to: selectedDate) {
            selectedDate = datePicker.date
        }
    }
    
    @IBAction func cancelDatePicker() {
        datePickerContainerView.isHidden = true
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.product = searchedProducts[indexPath.row]
        return cell
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        datePickerContainerView.isHidden = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            searchedProducts = products
        } else {
            searchedProducts = products.filter({ (product) -> Bool in
                return (product.name?.lowercased().contains(searchText.lowercased()) ?? false) ||
                    (product.tagline?.lowercased().contains(searchText.lowercased()) ?? false)
            })
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toProductComments", sender: searchedProducts[indexPath.row])
    }
}

extension ViewController {
    func setupDatePicker() {
        // You can view last 10 days posts
        datePicker.minimumDate = Calendar.current.date(byAdding: .day, value: -10, to: Date())
        datePicker.maximumDate = Date()
    }
    
    func setupTableView() {
        tableView.register(ProductTableViewCell.self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.refreshControl = refreshControl
        tableView.contentInset = UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 0)
    }
    
    func loadProducts() {
        products = []
        searchBar.isUserInteractionEnabled = false
        tableView.isUserInteractionEnabled = false
        dateBarButton.isEnabled = false
        isInErrorState = false
        
        if !refreshControl.isRefreshing {
            loader.startAnimating()
        }

        noProductLabel.isHidden = true

        Service().call("posts?day=" + selectedDate.asString, parameters: nil, type: .get) { (response, error, responseCode) in
            if let response = response, let rawPosts = response["posts"] as? [[String: Any]] {
                self.products = Product.models(from: rawPosts)
                
                if self.selectedDate.isEqualDayWise(to: Date()) {
                    let realm = try! Realm()
                    
                    try! realm.write {
                        realm.deleteAll()
                        realm.add(self.products)
                    }
                }
            } else if self.selectedDate.isEqualDayWise(to: Date()) {
                let realm = try! Realm()
                let products = Array(realm.objects(Product.self))
                
                let error = error as NSError?
                self.isInErrorState = error?.code == -1009
                
                self.products = products
            } else {
                self.isInErrorState = true
                self.products = []
            }
            
            self.loader.stopAnimating()
            self.searchBar.isUserInteractionEnabled = true
            self.tableView.isUserInteractionEnabled = true
            self.dateBarButton.isEnabled = true
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc func reloadProducts() {
        view.endEditing(true)
        loadProducts()
    }
}
