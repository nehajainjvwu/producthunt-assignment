//
//  Date.swift
//  Assignment
//
//  Created by Neha Jain on 18/01/20.
//

import Foundation

extension Date {
    var asString: String {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd" // 2017-09-13
            return formatter.string(from: self)
        }
    }
    
    func isEqualDayWise(to date: Date) -> Bool {
        return self.asString == date.asString
    }
}
